AshIdentity = LibStub("AceAddon-3.0"):NewAddon(
    "AshIdentity",
    "AceConsole-3.0",
    "AceHook-3.0",
    "AceEvent-3.0"
)

DEBUG_THIS = false

-- local _ = LibStub("AceLocale-3.0"):GetLocale("AshIdentity", true)

-- run once on addon load (i.e. after /reload)
function AshIdentity:OnInitialize()
    self:Print("AshIdentity initialised")

    -- https://www.wowace.com/projects/ace3/pages/ace-config-3-0-options-tables
    local options = {
        -- root node
        type = "group",
        name = "options.name",
        handler = self,
        args = {
            enabled = {
                order = 0,
                width = "full",
                name = "Enabled",
                desc = "Whether to perform substitutions",
                type = "toggle",
                set = function(_info, v)
                    self.db.profile.enabled = v
                end,
                get = function()
                    return self.db.profile.enabled
                end
            },
            format = {
                disabled = function() return not self.db.profile.enabled end,
                order = 10,
                name = "Format",
                desc = "Format string. id, message, zone, realzone, keylvl, keyzone",
                type = "input",
                set = function(_info, v)
                    self.db.profile.format = v
                end,
                get = function()
                    return self.db.profile.format
                end
            },
            id = {
                disabled = function() return not self.db.profile.enabled end,
                order = 11,
                name = "Identity",
                desc = "Value used for {id} sub",
                type = "input",
                set = function(_info, v)
                    self.db.profile.id = v
                end,
                get = function()
                    return self.db.profile.id
                end
            },
        }
    }

    -- register addon settings
    -- and add them to blizzard ui
    LibStub("AceConfig-3.0"):RegisterOptionsTable(
        "AshIdentity",
        options,
        {"/a-id", "/ash-identity"}
    )
    LibStub("AceConfigDialog-3.0"):AddToBlizOptions("AshIdentity", "Ash-Identity")

    -- https://www.wowace.com/projects/ace3/pages/api/ace-db-3-0
    local default_config = {
        profile = {
            enabled = true,
            format = "({id}) {message}",
            id = "its me"
        }
    }

    -- use AceDB to wrap SavedVariables
    self.db = LibStub("AceDB-3.0"):New("AshIdentityDB", default_config, true)
end

-- run whenever the addon is enabled,
-- even if the UI isn't /reloaded
function AshIdentity:OnEnable()
    -- sending a normal chat message
    self:RawHook("SendChatMessage", true)
    -- guilds/communities UI frame
    self:RawHook(
        C_Club,
        "SendMessage",
        "C_Club_SendMessage",
        true
    )
    -- idk, possibly sending bnet (light blue) whispers from normal and/or split chat frame?
    self:RawHook("BNSendWhisper", true)
end

-- actually modify the message with identity
-- this is where the magic happens
function AshIdentity:process(message, channel_name)
    if not self.db.profile.enabled then
        return message
    end

    local temp_channels = {
        ["GUILD"] = true
    }

    if not temp_channels[channel_name] then return message end

    if DEBUG_THIS then
        self:Printf("process(%s, %s)", message, channel_name)
    end

    -- todo: let users specify their own functions
    -- todo: add more fun keys:
    --  * mythic keystone link
    --  * ??? ???
    local temp_vars = {
        ["id"] = self.db.profile.id or "",
        ["message"] = message or "",
        ["zone"] = GetMinimapZoneText() or "",
        ["realzone"] = GetRealZoneText() or "",
        ["realm"] = GetRealmName(),
        ["nrealm"] = GetNormalizedRealmName(),
        ["class"] = select(1, UnitClass("player")),
        ["guild"] = select(1, GetGuildInfo("player")) or "",
        ["character"] = UnitName("player"),
        ["elevel"] = UnitEffectiveLevel("player"),
        ["level"] = UnitLevel("player"),
        ["keylvl"] = C_MythicPlus.GetOwnedKeystoneLevel() or "",
        ["keyzone"] = select(
            1,
            C_ChallengeMode.GetMapUIInfo(
                C_MythicPlus.GetOwnedKeystoneChallengeMapID() or 0
            )
        ) or ""
    }
    local modified = self.db.profile.format or ""
    for key, val in pairs(temp_vars) do
        local rx = "{" .. key .. "}"
        modified = modified:gsub(rx, val)
    end
    return modified
end

-- hook functions --
function AshIdentity:SendChatMessage(msg, type, lang, target)
    local orig = self.hooks["SendChatMessage"]
    local modified = self:process(msg, type)
    if DEBUG_THIS then
        self:Print(
            "SendChatMessage "
            .. msg
            .. " -> "
            .. modified
        )
    end
    return orig(modified, type, lang, target)
end
function AshIdentity:C_Club_SendMessage(club, stream, msg)
    local orig = self.hooks[C_Club]["SendMessage"]
    local modified = self:process(msg, "C_CLUB_SENDMESSAGE")
    if DEBUG_THIS then
        self:Print(
            "C_Club.SendMessage "
            .. msg
            .. " -> "
            .. modified
        )
    end
    return orig(club, stream, modified)
end
function AshIdentity:BNSendWhisper(acct, msg)
    local orig = self.hooks["BNSendWhisper"]
    local modified = self:process(msg, "BNET_SEND_WHISPER")
    if DEBUG_THIS then
        self:Print(
            "BNSendWhisper "
            .. msg
            .. " -> "
            .. modified
        )
    end
    return orig(acct, modified)
end
