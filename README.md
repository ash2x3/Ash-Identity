# Ash-Identity

A World of Warcraft addon to show your identity in chat. Created as an alternative to [Identity-2](https://www.curseforge.com/wow/addons/identity-2).

## Installation

Put these files in `<your wow directory>/_retail_/Interface/AddOns/Ash-Identity`.

## Usage

* Enable the addon in-game.
* Navigate to the main menu -> Interface -> AddOns -> Ash-Identity
* Write your format-string in the "Format" field
* (Optional) Write something in the "Identity" field for the `{id}` replacement to use

### Formatting

NOTE: This currently is hard-coded to only run on messages sent to guild chat!

Ash-Identity intercepts messages you send and replaces them with formatted strings. Unlike Identity-2, it does not automatically append the original message, so you need to do that yourself!

Format strings will replace certain sequences with special information. The current list of sequences are:
* `{id}`: text from the "Identity" option field
* `{message}`: original message text (!!)
* `{zone}`: zone name (from `GetMinimapZoneText()`)
* `{realzone}`: "real" zone name (from `GetRealZoneText()`)
* `{realm}`: realm name
* `{nrealm}`: normalised realm name (spaces etc. removed)
* `{class}`: your class name
* `{guild}`: your guild name
* `{character}`: your character name
* `{level}`: your character's level
* `{elevel}`: your character's "effective level" (can be scaled up/down by instances)
* `{keylvl}`: the level of the Mythic Keystone you have
* `{keyzone}`: the dungeon that the Mythic Keystone you have is for
